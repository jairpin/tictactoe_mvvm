package com.n3k0.tictactoe.model;

/**
 * Created by N3k0 on 08/14/17.
 */

public class Cell {

    private Player value;

    public Player getValue() {
        return value;
    }

    public void setValue(Player value) {
        this.value = value;
    }
}
